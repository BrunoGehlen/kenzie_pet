from django.contrib import admin
from django.urls import path
from .views import AnimalView, RetrieveDestroyAnimalView

urlpatterns = [
    path('animals/', AnimalView.as_view()),
    path('animals/<int:pk>/', RetrieveDestroyAnimalView.as_view())
]
