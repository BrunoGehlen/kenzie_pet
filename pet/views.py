from .models import Animal
from .serializers import AnimalSerializer
from rest_framework import generics
from rest_framework.mixins import ListModelMixin


class MultipleFieldLookupMixin:
    def get_queryset(self):
        queryset = self.queryset
        lookup_filter = {}
        for lookup_field in self.lookup_fields:
            if self.request.data.get(lookup_field):
                lookup_filter[f"{lookup_field}__icontains"] = self.request.data.get(lookup_field)
            queryset = queryset.filter(**lookup_filter)
            return queryset


class AnimalView(generics.ListCreateAPIView, MultipleFieldLookupMixin):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer
    lookup_fields = ["name"]


class RetrieveDestroyAnimalView(generics.RetrieveDestroyAPIView):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer

