from rest_framework import serializers
from .models import Animal, Characteristic, Group

class CharacteristicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Characteristic
        fields = ['id', 'characteristic']

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name', 'scientific_name']


class AnimalSerializer(serializers.ModelSerializer):
    group = GroupSerializer()
    characteristic_set = CharacteristicSerializer(many=True)

    class Meta:
        model = Animal
        fields = [
                'id',
                'name', 
                'age', 
                'weight', 
                'sex', 
                'group', 
                'characteristic_set'
            ]

    def create(self, validated_data, *args, **kwargs):
        group = validated_data.pop('group')
        group = Group.objects.get_or_create(**group)[0]
        characteristic_set = validated_data.pop('characteristic_set')
        
        animal = Animal.objects.create(**validated_data, group=group)

        for characteristic in characteristic_set:
            char = Characteristic.objects.get_or_create(
                **characteristic)[0]
            char.animal.add(animal)

        return animal
